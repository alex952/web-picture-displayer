# README #

Flask web application that creates a dynamic slideshow off pictures from different sources (initially just gmail attachements).

## Installation ##
1. Create a virtual environment for the project with Python 3
2. Install requirements
```
#!bash

pip install -r requirements.txt
```

3. Launch index.py

```
#!bash

python index.py
```

