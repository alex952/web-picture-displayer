from models import Picture, User
from flask.ext.login import current_user
from flask import current_app

import os
from pprint import pprint
import base64
from datetime import datetime

import simplejson as json



# Test if can connect to the db outside the app context
import sqlalchemy as sa


engine = None
sessionmaker = sa.orm.sessionmaker()
session = sa.orm.scoped_session(sessionmaker)


class Downloader():

    def __init__(self, user):
        self._user = user

    def fetchNextBatch(self):
        pass

class GoogleDownloader(Downloader):

    MAILS_ENDPOINT = 'https://www.googleapis.com/gmail/v1/users/me/messages'
    MAIL_ENDPOINT = MAILS_ENDPOINT + '/{0}'
    MAIL_MODIFY_ENDPOINT = MAILS_ENDPOINT + '/{0}/modify'
    ATTACHMENTS_ENDPOINT = MAIL_ENDPOINT + '/attachments/{1}'


    def __init__(self, user, oauth_session):
        super(GoogleDownloader, self).__init__(user)
        self._session = oauth_session

    def fetchNextBatch(self):
        global sessionmaker, engine, session


        ret = self._session.get(GoogleDownloader.MAILS_ENDPOINT, params={'q': 'is:unread'}).json()

        if 'messages' not in ret:
            return

        message_ids = []
        messages = ret['messages']

        for m in messages:
            m_id = m['id']
            message_ids.append(m_id)
            m_ret = self._session.get(GoogleDownloader.MAIL_ENDPOINT.format (m_id)).json()

            parts = m_ret['payload']['parts']

            for p in parts:
                if p['mimeType'] == 'image/jpeg':
                    att_filename = p['filename']
                    att_id = p['body']['attachmentId']
                    att_ret = self._session.get(GoogleDownloader.ATTACHMENTS_ENDPOINT.format (m_id, att_id)).json()
                    file_data = base64.urlsafe_b64decode(att_ret['data'].encode('UTF-8'))

                    tt = datetime.strftime(datetime.utcnow(), '%H:%M:%S')
                    att_filename_parts = os.path.splitext(att_filename)
                    att_filename = '{0}-{1}{2}'.format(att_filename_parts[0], tt, att_filename_parts[1])

                    pic_filepath = os.path.join(self._user.folder, att_filename)

                    f = open(pic_filepath, 'wb')
                    f.write(file_data)
                    f.close()

                    engine = sa.create_engine('sqlite:////home/alex952/web-displayer.db')
                    session.remove()
                    sessionmaker.configure(bind=engine)

                    pic = Picture(pic_filepath, self._user.username)

                    session.add(pic)
                    session.commit()


            for m_id in message_ids:
                json_data = json.dumps({'addLabelIds': [], "removeLabelIds": ["UNREAD"]})
                self._session.post(GoogleDownloader.MAIL_MODIFY_ENDPOINT.format (m_id), data = json_data, headers={'Content-Type': 'application/json'})
