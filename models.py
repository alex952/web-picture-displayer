from datetime import datetime
import os

from flask.ext.sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class User(db.Model):

    # BASE_FOLDER = os.path.expanduser('~/.web-picture-displayer')

    username = db.Column(db.String(50), unique = True)
    email = db.Column(db.String(50), primary_key = True)
    authenticated = db.Column(db.Boolean, default = False)
    folder = db.Column(db.String(100), unique = True)

    pictures = db.relationship('Picture', backref = 'user', lazy = 'dynamic')

    def __init__(self, email, path):
        self.username = email.split('@')[0]
        self.email = email
        self.folder = os.path.join(path, self.username)

    # flask login specific
    def is_active(self):
        return True

    def get_id(self):
        return self.email

    def is_authenticated(self):
        return self.authenticated

    def is_anonymous(self):
        return False

class Picture(db.Model):
    path = db.Column(db.String(150), primary_key = True)
    date_added = db.Column(db.DateTime, default = datetime.utcnow)
    showed = db.Column(db.Boolean, default = False)

    user_id = db.Column(db.String(50), db.ForeignKey('user.username'))


    def __init__(self, path, user_id):
        self.path = path
        self.user_id = user_id
