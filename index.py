from flask import Flask, render_template, send_from_directory, jsonify
from flask.ext.login import login_user, logout_user, current_user, login_required, LoginManager
from auth import *
from apscheduler.schedulers.background import BackgroundScheduler
import logging
import os

from sqlalchemy.sql.expression import func
from sqlalchemy import desc

from models import User, db, Picture

from downloader import GoogleDownloader as downloader

from pprint import pprint

scheduler = None
jobs = {}

app = Flask(__name__)
app.secret_key = 'o,aiQIEer29sntaoirwwii!!@Inaoire'
app.config.from_object('config')

login_manager = LoginManager()
login_manager.init_app(app)

db.init_app(app)

PICS_FOLDER = os.path.join(app.config['STATIC_FOLDER'], app.config['PICS_BASE'])

@app.route("/")
def index():
    if current_user.is_anonymous():
        return render_template('landpage.html')
    else:
        return render_template('index.html')

@app.route('/authorize/<provider>')
def oauth_authorize(provider):
    oauth = OAuthSignIn.get_provider(provider)
    return oauth.authorize()

@app.route('/callback/<provider>')
def oauth_callback(provider):
    oauth = OAuthSignIn.get_provider(provider)
    email, session = oauth.callback()

    if email is None:
        flash('Authentication failed.')
        return redirect(url_for('index'))

    user = User.query.get(email)

    if not user:
        user = User(email, PICS_FOLDER)
        os.makedirs(user.folder, exist_ok = True)

    user.authenticated = True

    db.session.add(user)
    db.session.commit()

    d = downloader(user, session)
    job = scheduler.add_job(d.fetchNextBatch, 'interval', seconds=10)
    jobs[user.email] = job

    login_user(user, remember = True)

    return redirect(url_for('index'))

@app.route('/logout')
def logout():
    if not current_user.is_anonymous():
        u = User.query.get(current_user.email)
        u.authenticated = False

        db.session.add(u)
        db.session.commit()

        if u.email in jobs:
            jobs[u.email].remove()
            del jobs[u.email]

        logout_user()

    return redirect(url_for('index'))

@app.route('/next_image')
def nextImage():
    if current_user.is_anonymous():
        return ('', 204)

    pic = Picture.query \
                 .filter_by(showed=False) \
                 .filter_by(user_id=current_user.username) \
                 .order_by(Picture.date_added) \
                 .limit(1) \
                 .all()

    if not pic:
        pic = Picture.query \
                .filter_by(user_id=current_user.username) \
                .order_by(func.random()) \
                .limit(1) \
                .all()

        if not pic:
            return ('', 204)

    pic = pic[0]
    pic.showed = True
    db.session.add(pic)
    db.session.commit()

    return jsonify({'path': pic.path})

@login_manager.user_loader
def user_loader(user_id):
    return User.query.get(user_id)

if __name__ == "__main__":

    # Create the folder for the pics
    os.makedirs(PICS_FOLDER, exist_ok = True)

    # Create scheduler and start it
    scheduler = BackgroundScheduler()
    scheduler.start()

    # Run the app
    app.run(host = '0.0.0.0', debug = True)
