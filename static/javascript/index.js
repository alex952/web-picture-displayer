var f;

$(document).ready(function () {
    var $fotoramaDiv = $('#fotorama').fotorama({
        allowfullscreen: 'native',
        nav: false,
        arrows: false,
        autoplay: false,
        maxheight: "90%"
    });
    f = $fotoramaDiv.data('fotorama');


    window.setInterval(function () {
        $.get('/next_image', function (data) {
            if (data) {
                console.log(data);

                f.push({img: data.path});
                f.show('>>')
                console.log(f);
            }
        });
    }, 3000);
});
